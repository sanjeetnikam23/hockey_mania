// ignore_for_file: file_names, unused_local_variable

import 'dart:developer';
import 'dart:io';

import 'package:hockey_mania/hockey_model.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static Database? _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database?> get database async {
    // If database exists, return database
    if (_database != null) return _database;

    // If database don't exists, create one
    _database = await initDB();

    return _database;
  }

  // Create the database and the Employee table
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'hockey_mania3.db');

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      //await db.execute('DROP TABLE Pro');
      await db.execute('CREATE TABLE sample('
          'StartTime TEXT,'
          'EndTime DATETIME,'
          'City TEXT,'
          'TeamName TEXT,'
          'Score INTEGER,'
          'Yard INTEGER,'
          'DatePosted DATETIME,'
          'RankExtended TEXT,'
          'Abbreviation TEXT'
          ')');
    });
  }

  // Insert employee on database
  createEmployee(TicketInformationTeam newEmployee) async {
    //await deleteAllEmployees();
    final db = await database;

   
    final res = await db?.insert('sample', newEmployee.toJson());
    //log("res:$res");
    return res;
  }

  Future<List<TicketInformationTeam>> getAllEmployees() async {
    final db = await database;
    final res = await db?.rawQuery("SELECT * FROM sample");
    List<TicketInformationTeam> list =
        res!.isNotEmpty ? res.map((c) => TicketInformationTeam.fromJson(c)).toList() : [];
    return list;
  }

  Future<List<TicketInformationTeam>> searchEmployees(String search) async {
    final db = await database;
    final res = await db?.rawQuery(
        "SELECT * FROM sample WHERE family_name LIKE '%$search%' OR family_address LIKE '%$search%' OR family_id LIKE '%$search%' OR family_members LIKE '%$search%'");
    // log("id:${res.runtimeType}-->${res!.length}");

    List<TicketInformationTeam> list =
        res!.isNotEmpty ? res.map((c) => TicketInformationTeam.fromJson(c)).toList() : [];
    return list;
  }

  void deletedata() async {
    Database? db = await database;
    await (db as dynamic).rawQuery("Delete from sample");
    log("Data Deleted");
  }

  Future<List<Map<String, dynamic>>> queryall() async {
    Database? db = await database;
    return await (db as dynamic).rawQuery("SELECT * from sample");
  }
}
