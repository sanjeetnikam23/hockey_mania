// ignore_for_file: avoid_print, unused_import


import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:hockey_mania/Database/DatabaseHelper.dart';
import 'package:hockey_mania/Database/Databse.dart';
import 'package:hockey_mania/hockey_model.dart';
import 'package:http/http.dart';

class HockeyMania extends StatefulWidget {
  const HockeyMania({Key? key}) : super(key: key);

  @override
  State<HockeyMania> createState() => _HockeyManiaState();
}

class _HockeyManiaState extends State<HockeyMania> {
  List<dynamic> todo = [];
  List<TicketInformationTeam> temp = [];
  Future<bool> fetchData() async {
    final response = await post(Uri.parse(
        'http://www.charitymania.com/cmservices/footballsvcjson.aspx?Tickets=1-2H001&Source=%22iOS%22'));
    if (response.statusCode == 200) {
      final result = hockeyManiaFromJson(response.body);
      for (int i = 0; i < result.ticketInformation[0].teams.length; i++) {
       temp.add(result.ticketInformation[0].teams[i]);
      }
      //log(temp.length.toString());
      setState(() {
        
      });
      return true;
    } else {
      return false;
    }
  }
  DatabseHelper databseHelper = DatabseHelper();
    
  @override
  void initState() {
    super.initState();
    fetchData();
    databseHelper.getDatabase();
    databseHelper.printdata(); 
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hockey Mania'),
        backgroundColor: Colors.deepOrangeAccent,
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(temp[index].teamName),
            subtitle: Text(temp[index].city),
            leading: Text(temp[index].rankExtended),
            trailing: Text(temp[index].abbreviation),
          );
        },
        itemCount: temp.length,
      ),
    );
  }
}
