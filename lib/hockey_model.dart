// To parse this JSON data, do
//
//     final hockeyMania = hockeyManiaFromJson(jsonString);

import 'dart:convert';

HockeyMania hockeyManiaFromJson(String str) => HockeyMania.fromJson(json.decode(str));

String hockeyManiaToJson(HockeyMania data) => json.encode(data.toJson());

class HockeyMania {
    HockeyMania({
        required this.ticketInformation,
        required this.gameInformation,
    });

    List<TicketInformation> ticketInformation;
    List<GameInformation> gameInformation;

    factory HockeyMania.fromJson(Map<String, dynamic> json) => HockeyMania(
        ticketInformation: List<TicketInformation>.from(json["TicketInformation"].map((x) => TicketInformation.fromJson(x))),
        gameInformation: List<GameInformation>.from(json["GameInformation"].map((x) => GameInformation.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "TicketInformation": List<dynamic>.from(ticketInformation.map((x) => x.toJson())),
        "GameInformation": List<dynamic>.from(gameInformation.map((x) => x.toJson())),
    };
}

class GameInformation {
    GameInformation({
        required this.game,
        required this.totalTicketCount,
        required this.resultsWeek,
        required this.hi,
        required this.lo,
        required this.scoreByTeam,
    });

    String game;
    int totalTicketCount;
    int resultsWeek;
    Hi hi;
    Hi lo;
    List<ScoreByTeam> scoreByTeam;

    factory GameInformation.fromJson(Map<String, dynamic> json) => GameInformation(
        game: json["Game"],
        totalTicketCount: json["TotalTicketCount"],
        resultsWeek: json["ResultsWeek"],
        hi: Hi.fromJson(json["Hi"]),
        lo: Hi.fromJson(json["Lo"]),
        scoreByTeam: List<ScoreByTeam>.from(json["ScoreByTeam"].map((x) => ScoreByTeam.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "Game": game,
        "TotalTicketCount": totalTicketCount,
        "ResultsWeek": resultsWeek,
        "Hi": hi.toJson(),
        "Lo": lo.toJson(),
        "ScoreByTeam": List<dynamic>.from(scoreByTeam.map((x) => x.toJson())),
    };
}

class Hi {
    Hi({
        required this.grandPrizeWinners,
        required this.ticketTeams,
    });

    List<GrandPrizeWinner> grandPrizeWinners;
    List<TicketTeam> ticketTeams;

    factory Hi.fromJson(Map<String, dynamic> json) => Hi(
        grandPrizeWinners: List<GrandPrizeWinner>.from(json["GrandPrizeWinners"].map((x) => GrandPrizeWinner.fromJson(x))),
        ticketTeams: List<TicketTeam>.from(json["TicketTeams"].map((x) => TicketTeam.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "GrandPrizeWinners": List<dynamic>.from(grandPrizeWinners.map((x) => x.toJson())),
        "TicketTeams": List<dynamic>.from(ticketTeams.map((x) => x.toJson())),
    };
}

class GrandPrizeWinner {
    GrandPrizeWinner({
        required this.ticketNumber,
        required this.totalScore,
        required this.totalTie,
    });

    int ticketNumber;
    int totalScore;
    int totalTie;

    factory GrandPrizeWinner.fromJson(Map<String, dynamic> json) => GrandPrizeWinner(
        ticketNumber: json["TicketNumber"],
        totalScore: json["TotalScore"],
        totalTie: json["TotalTie"],
    );

    Map<String, dynamic> toJson() => {
        "TicketNumber": ticketNumber,
        "TotalScore": totalScore,
        "TotalTie": totalTie,
    };
}

class TicketTeam {
    TicketTeam({
        required this.weekNumber,
        required this.tickets,
    });

    int weekNumber;
    List<Ticket> tickets;

    factory TicketTeam.fromJson(Map<String, dynamic> json) => TicketTeam(
        weekNumber: json["WeekNumber"],
        tickets: List<Ticket>.from(json["Tickets"].map((x) => Ticket.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "WeekNumber": weekNumber,
        "Tickets": List<dynamic>.from(tickets.map((x) => x.toJson())),
    };
}

class Ticket {
    Ticket({
        required this.ticketNumber,
        required this.ticketScore,
        required this.totalTie,
        required this.teamList,
        required this.rank,
    });

    int ticketNumber;
    int ticketScore;
    int totalTie;
    String teamList;
    int rank;

    factory Ticket.fromJson(Map<String, dynamic> json) => Ticket(
        ticketNumber: json["TicketNumber"],
        ticketScore: json["TicketScore"],
        totalTie: json["TotalTie"],
        teamList: json["TeamList"],
        rank: json["Rank"],
    );

    Map<String, dynamic> toJson() => {
        "TicketNumber": ticketNumber,
        "TicketScore": ticketScore,
        "TotalTie": totalTie,
        "TeamList": teamList,
        "Rank": rank,
    };
}

class ScoreByTeam {
    ScoreByTeam({
        required this.weekNumber,
        required this.teams,
    });

    String weekNumber;
    List<ScoreByTeamTeam> teams;

    factory ScoreByTeam.fromJson(Map<String, dynamic> json) => ScoreByTeam(
        weekNumber: json["WeekNumber"],
        teams: List<ScoreByTeamTeam>.from(json["Teams"].map((x) => ScoreByTeamTeam.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "WeekNumber": weekNumber,
        "Teams": List<dynamic>.from(teams.map((x) => x.toJson())),
    };
}

class ScoreByTeamTeam {
    ScoreByTeamTeam({
        required this.city,
        required this.score,
        required this.tieBreaker,
    });

    String city;
    int score;
    int tieBreaker;

    factory ScoreByTeamTeam.fromJson(Map<String, dynamic> json) => ScoreByTeamTeam(
        city: json["City"],
        score: json["Score"],
        tieBreaker: json["TieBreaker"],
    );

    Map<String, dynamic> toJson() => {
        "City": city,
        "Score": score,
        "TieBreaker": tieBreaker,
    };
}

class TicketInformation {
    TicketInformation({
        required this.ticketNumber,
        required this.teams,
        required this.yourScore,
        required this.details,
    });

    String ticketNumber;
    List<TicketInformationTeam> teams;
    YourScore yourScore;
    Details details;

    factory TicketInformation.fromJson(Map<String, dynamic> json) => TicketInformation(
        ticketNumber: json["TicketNumber"],
        teams: List<TicketInformationTeam>.from(json["Teams"].map((x) => TicketInformationTeam.fromJson(x))),
        yourScore: YourScore.fromJson(json["YourScore"]),
        details: Details.fromJson(json["Details"]),
    );

    Map<String, dynamic> toJson() => {
        "TicketNumber": ticketNumber,
        "Teams": List<dynamic>.from(teams.map((x) => x.toJson())),
        "YourScore": yourScore.toJson(),
        "Details": details.toJson(),
    };
}

class Details {
    Details({
        required this.orgName,
        required this.orgPhone,
        required this.street,
        required this.city,
        required this.state,
        required this.postalCode,
    });

    String orgName;
    String orgPhone;
    String street;
    String city;
    String state;
    String postalCode;

    factory Details.fromJson(Map<String, dynamic> json) => Details(
        orgName: json["OrgName"],
        orgPhone: json["OrgPhone"],
        street: json["Street"],
        city: json["City"],
        state: json["State"],
        postalCode: json["PostalCode"],
    );

    Map<String, dynamic> toJson() => {
        "OrgName": orgName,
        "OrgPhone": orgPhone,
        "Street": street,
        "City": city,
        "State": state,
        "PostalCode": postalCode,
    };
}

class TicketInformationTeam {
    TicketInformationTeam({
        required this.startTime,
        required this.endTime,
        required this.city,
        required this.teamName,
        required this.score,
        required this.yard,
        required this.datePosted,
        required this.rankExtended,
        required this.abbreviation,
    });

    DateTime startTime;
    DateTime endTime;
    String city;
    String teamName;
    int score;
    int yard;
    DateTime datePosted;
    String rankExtended;
    String abbreviation;

    factory TicketInformationTeam.fromJson(Map<String, dynamic> json) => TicketInformationTeam(
        startTime: DateTime.parse(json["StartTime"]),
        endTime: DateTime.parse(json["EndTime"]),
        city: json["City"],
        teamName: json["TeamName"],
        score: json["Score"],
        yard: json["Yard"],
        datePosted: DateTime.parse(json["DatePosted"]),
        rankExtended: json["RankExtended"],
        abbreviation: json["Abbreviation"],
    );

    Map<String, dynamic> toJson() => {
        "StartTime": startTime.toIso8601String(),
        "EndTime": endTime.toIso8601String(),
        "City": city,
        "TeamName": teamName,
        "Score": score,
        "Yard": yard,
        "DatePosted": datePosted.toIso8601String(),
        "RankExtended": rankExtended,
        "Abbreviation": abbreviation,
    };
}

class YourScore {
    YourScore({
        required this.totalScore,
        required this.totalYards,
        required this.ticketRank,
        required this.datePosted,
    });

    int totalScore;
    int totalYards;
    int ticketRank;
    DateTime datePosted;

    factory YourScore.fromJson(Map<String, dynamic> json) => YourScore(
        totalScore: json["TotalScore"],
        totalYards: json["TotalYards"],
        ticketRank: json["TicketRank"],
        datePosted: DateTime.parse(json["DatePosted"]),
    );

    Map<String, dynamic> toJson() => {
        "TotalScore": totalScore,
        "TotalYards": totalYards,
        "TicketRank": ticketRank,
        "DatePosted": datePosted.toIso8601String(),
    };
}
